package controller;

import java.net.URL;
import java.util.*;
import javafx.fxml.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.canvas.*;
import javafx.scene.control.*;
import javafx.scene.control.Alert.*;
import javafx.scene.image.*;
import javafx.scene.input.*;
import javafx.scene.paint.*;
import javafx.scene.shape.StrokeLineCap;
import javafx.application.Platform;
import javafx.event.*;
import model.*;

public class DrawDigitViewController implements Initializable {

	private LabelledImageArrayLoader currentLIArrayLoader = null;
	private Scene parentMainScene = null;
	private ImageView currentImageView = null;
	private GraphicsContext currentGContext = null;
	
	@FXML
    private Button compareImageButton;
	
	@FXML
    private Button backButton;

    @FXML
    private Label digitStatusLabel;

    @FXML
    private Label confidenceStatusLabel;
    
    @FXML
    private Label kValueLabel;
    
    @FXML
    private TextField kValueTextField;
    
    @FXML
    private Canvas ddvCanvas;
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		 
	}
	
	public void setLIALoader(LabelledImageArrayLoader passedLIALoader) {
		this.currentLIArrayLoader = passedLIALoader;
	}
	
	public void setParentScene (Scene providedParentScene) {
		this.parentMainScene = providedParentScene;
	}
	
	public void setCanvasControls() {
		try {
			currentImageView = new ImageView();
			currentGContext = ddvCanvas.getGraphicsContext2D();
			//Set the image view to the canvas
			currentImageView.setFitHeight(ddvCanvas.getHeight() - 60);
			currentImageView.setFitWidth(ddvCanvas.getWidth() - 60);
			//Set line width and stroke type for drawing
			currentGContext.setLineWidth(50);
			currentGContext.setLineCap(StrokeLineCap.ROUND);
		} catch (Exception ex) {
			Alert warningAlert = new Alert(AlertType.ERROR);
			warningAlert.setTitle("Exception!");
			warningAlert.setHeaderText("Exception: " + ex.toString());
			warningAlert.show();
		}
	}
	
	@FXML
    void ddvCanvasMousePressed(MouseEvent event) {
		try {
			currentGContext.setStroke(Color.BLACK);
			currentGContext.beginPath();
			//set stroke to actual mouse position at x and y
			currentGContext.moveTo(event.getSceneX() - currentGContext.getLineWidth(), 
					event.getSceneY() - (2 * currentGContext.getLineWidth()));
			currentGContext.stroke();
		} catch (Exception ex) {
			Alert warningAlert = new Alert(AlertType.ERROR);
			warningAlert.setTitle("Exception!");
			warningAlert.setHeaderText("Exception: " + ex.toString());
			warningAlert.show();
		}
    }
	
	@FXML
	void ddvCanvasMouseDragged(MouseEvent event) {
		try {
			//set stroke to actual mouse position at x and y
			currentGContext.lineTo(event.getSceneX() - currentGContext.getLineWidth(), 
					event.getSceneY() - (2 * currentGContext.getLineWidth()));
			currentGContext.stroke();
		} catch (Exception ex) {
			Alert warningAlert = new Alert(AlertType.ERROR);
			warningAlert.setTitle("Exception!");
			warningAlert.setHeaderText("Exception: " + ex.toString());
			warningAlert.show();
		}
    }
	
	@FXML
	void clearButtonClicked(ActionEvent event) {
		try {
			currentGContext.clearRect(0, 0, ddvCanvas.getWidth(), ddvCanvas.getHeight());
			digitStatusLabel.setText("");
			confidenceStatusLabel.setText("");
		} catch (Exception ex) {
			Alert warningAlert = new Alert(AlertType.ERROR);
			warningAlert.setTitle("Exception!");
			warningAlert.setHeaderText("Exception: " + ex.toString());
			warningAlert.show();
		}
	}
	
	@FXML
	void compareImageButtonClicked(ActionEvent event) {
		digitStatusLabel.setVisible(false);
		confidenceStatusLabel.setVisible(false);
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				try {
					int providedKValue = 0;
					if (kValueTextField.getText() != "") {
						providedKValue = Integer.parseInt(kValueTextField.getText());
					} else {
						Alert warningAlert = new Alert(AlertType.WARNING);
						warningAlert.setTitle("Warning!");
						warningAlert.setHeaderText("No K-Value provided!");
						warningAlert.show();
					}
					if (providedKValue > 0 & providedKValue <= currentLIArrayLoader.getLoadedLabelCount()) {
						digitStatusLabel.setText("Processing...");
						digitStatusLabel.setVisible(true);				
						WritableImage currentWImage = new WritableImage((int)ddvCanvas.getWidth(), (int)ddvCanvas.getHeight());
						ddvCanvas.snapshot(null, currentWImage);
						currentLIArrayLoader.setIOperatorImageFromCanvas(currentWImage);
						if (currentLIArrayLoader.getIOperatorImage() != null) {
							currentLIArrayLoader.computeEDistanceForArrayList();
							double imageConfidence = currentLIArrayLoader.getConfidenceRatio(providedKValue);
							int recognizedDigit = currentLIArrayLoader.getRecognizedDigit();
							//display confidence feedback
							digitStatusLabel.setText("The digit is: " + recognizedDigit);
							digitStatusLabel.setVisible(true);
							confidenceStatusLabel.setText("The confidence of digit recognition is: " + imageConfidence + "%");
							confidenceStatusLabel.setVisible(true);
						} else {
							Alert warningAlert = new Alert(AlertType.WARNING);
							warningAlert.setTitle("Warning!");
							warningAlert.setHeaderText("Image has not been set!");
							warningAlert.show();
						}
					} else {
						Alert warningAlert = new Alert(AlertType.WARNING);
						warningAlert.setTitle("Warning!");
						warningAlert.setHeaderText("K-Value provided is out of range!");
						warningAlert.show();
					}
				} catch (NullPointerException npe) {
					Alert warningAlert = new Alert(AlertType.ERROR);
					warningAlert.setTitle("Exception!");
					warningAlert.setHeaderText("Exception: " + npe.toString());
					warningAlert.show();
				} catch (Exception ex) {
					Alert warningAlert = new Alert(AlertType.ERROR);
					warningAlert.setTitle("Exception!");
					warningAlert.setHeaderText("Exception: " + ex.toString());
					warningAlert.show();
				}
			}
		});
	}
	
	@FXML
	void backButtonClicked(ActionEvent event) {
		try {
			if (parentMainScene != null) {
	    		Stage currentStage = (Stage)backButton.getScene().getWindow();
	    		currentStage.setScene(parentMainScene);
			}
    	} catch (Exception ex) {
    		Alert warningAlert = new Alert(AlertType.ERROR);
			warningAlert.setTitle("Exception!");
			warningAlert.setHeaderText("Exception: " + ex.toString());
			warningAlert.show();
    	}
	}
}
