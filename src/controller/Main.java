package controller;
	
import javafx.application.Application;
import javafx.scene.*;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import javafx.fxml.*;

public class Main extends Application {
	
	//private LabelledImageArrayLoader mainLIALoader = null;
	
	@Override
	public void start(Stage primaryStage) {
		try {
			//mainLIALoader = new LabelledImageArrayLoader();
			FXMLLoader currentFXMLLoader = new FXMLLoader(getClass().getResource("../view/mainview.fxml"));
			Scene currentScene = new Scene((FlowPane)currentFXMLLoader.load());
			currentScene.getStylesheets().add("view/application.css");
			MainViewController currentMVController = currentFXMLLoader.<MainViewController>getController();
			currentMVController.initializeAppLoad();
			primaryStage.setScene(currentScene);
			primaryStage.show();
		} catch(Exception ex) {
			Alert warningAlert = new Alert(AlertType.ERROR);
			warningAlert.setTitle("Exception!");
			warningAlert.setHeaderText("Exception: " + ex.toString());
			warningAlert.show();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
