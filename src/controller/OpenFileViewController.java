package controller;

import java.net.URL;
import java.io.*;
import java.util.*;
import javafx.fxml.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.canvas.*;
import javafx.scene.control.*;
import javafx.scene.control.Alert.*;
import javafx.scene.image.*;
import javafx.event.*;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import model.*;

public class OpenFileViewController implements Initializable {
	
	private LabelledImageArrayLoader currentLIArrayLoader = null;
	private File imageFile = null;
	private ImageFileHandler currentIFHandler = null;
	private String selectedFilePath = null;
	private Scene parentMainScene = null;
	private GraphicsContext currentGContext = null;
	
	@FXML
    private Button selectFileButton;
	
	@FXML
    private Button compareFileButton;
	
	@FXML
    private Button cancelButton;
	
	@FXML
	private Button backButton;
	
	@FXML
    private Label digitStatusLabel;

    @FXML
    private Label confidenceStatusLabel;

    @FXML
    private Label fileLocationLabel;
    
    @FXML
    private Label kValueLabel;
    
    @FXML
    private TextField fileLocationTextField;
    
    @FXML
    private TextField kValueTextField;
    
    @FXML
    private Canvas ofvCanvas;
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		
	}
	
	public void setLIALoader(LabelledImageArrayLoader passedLIALoader) {
		this.currentLIArrayLoader = passedLIALoader;
	}
	
	public void setParentScene(Scene providedParentScene) {
		this.parentMainScene = providedParentScene;
	}
	
	@FXML
	void selectFileButtonClicked(ActionEvent event) {
		try {
			currentGContext = ofvCanvas.getGraphicsContext2D();
			currentGContext.clearRect(0, 0, ofvCanvas.getWidth(), ofvCanvas.getHeight());
			FileChooser newFileChooser = new FileChooser();
			newFileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("JPG", "*.jpg"));
			imageFile = newFileChooser.showOpenDialog(null);
			if (imageFile != null) {
				fileLocationTextField.setText(imageFile.getAbsolutePath());
				selectedFilePath = imageFile.getAbsolutePath();
				currentIFHandler = new ImageFileHandler(selectedFilePath);
				currentIFHandler.readFile();
				currentLIArrayLoader.setIOperatorImage(currentIFHandler.getImage());
				currentLIArrayLoader.getIOperator().centerAndPadSetImage(currentLIArrayLoader.getIOperatorImage());
				int functionalWidth = (int)ofvCanvas.getWidth();
				int functionalHeight = (int)ofvCanvas.getHeight();
				if (currentLIArrayLoader.getIOperator().getImage().getWidth() < functionalWidth) {
					functionalWidth = currentLIArrayLoader.getIOperator().getImage().getWidth();
				}
				if (currentLIArrayLoader.getIOperator().getImage().getHeight() < functionalHeight) {
					functionalHeight = currentLIArrayLoader.getIOperator().getImage().getHeight();
				}
				currentLIArrayLoader.getIOperator().resizeSetImage(currentLIArrayLoader.getIOperatorImage(), functionalWidth, functionalHeight);
				Image currentLoadedImage = SwingFXUtils.toFXImage(currentLIArrayLoader.getIOperator().getImage(), null);
				currentGContext.drawImage(currentLoadedImage, 0, 0);
			}
		} catch (IOException ioe) {
			Alert warningAlert = new Alert(AlertType.ERROR);
			warningAlert.setTitle("Exception!");
			warningAlert.setHeaderText("Exception: " + ioe.toString());
			warningAlert.show();
		} catch (Exception ex) {
			Alert warningAlert = new Alert(AlertType.ERROR);
			warningAlert.setTitle("Exception!");
			ex.printStackTrace();
			warningAlert.show();
		}
	}	
	
	@FXML
	void compareFileButtonClicked(ActionEvent event) {
		digitStatusLabel.setVisible(false);
		confidenceStatusLabel.setVisible(false);
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				try {
					int providedKValue = 0;
					if (kValueTextField.getText() != "") {
						providedKValue = Integer.parseInt(kValueTextField.getText());
					} else {
						Alert warningAlert = new Alert(AlertType.WARNING);
						warningAlert.setTitle("Warning!");
						warningAlert.setHeaderText("No K-Value provided!");
						warningAlert.show();
					}
					if (providedKValue > 0 & providedKValue <= currentLIArrayLoader.getLoadedLabelCount()) {
						if (selectedFilePath != null) {
							digitStatusLabel.setText("Processing...");
							digitStatusLabel.setVisible(true);
							currentLIArrayLoader.setIOperatorImage(currentIFHandler.getImage());
							currentLIArrayLoader.getIOperator().centerAndPadSetImage(currentLIArrayLoader.getIOperatorImage());
							currentLIArrayLoader.getIOperator().convertRGBToGreyscale(currentLIArrayLoader.getIOperatorImage());				
							currentLIArrayLoader.getIOperator().resizeSetImage(currentLIArrayLoader.getIOperatorImage(), 28, 28);
							currentLIArrayLoader.computeEDistanceForArrayList();
							double imageConfidence = currentLIArrayLoader.getConfidenceRatio(providedKValue);
							int recognizedDigit = currentLIArrayLoader.getRecognizedDigit();
							//display confidence feedback
							digitStatusLabel.setText("The digit is: " + recognizedDigit);
							digitStatusLabel.setVisible(true);
							confidenceStatusLabel.setText("The confidence of digit recognition is: " + imageConfidence + "%");
							confidenceStatusLabel.setVisible(true);
						} else {
							Alert warningAlert = new Alert(AlertType.WARNING);
							warningAlert.setTitle("Warning!");
							warningAlert.setHeaderText("No file selected!");
							warningAlert.show();
						}
					} else {
						Alert warningAlert = new Alert(AlertType.WARNING);
						warningAlert.setTitle("Warning!");
						warningAlert.setHeaderText("K-Value provided is out of range!");
						warningAlert.show();
					}
				} catch (NullPointerException npe) {
					Alert warningAlert = new Alert(AlertType.ERROR);
					warningAlert.setTitle("Exception!");
					warningAlert.setHeaderText("Exception: " + npe.toString());
					warningAlert.show();
				} catch (Exception ex) {
					Alert warningAlert = new Alert(AlertType.ERROR);
					warningAlert.setTitle("Exception!");
					warningAlert.setHeaderText("Exception: " + ex.toString());
					warningAlert.show();
				}
			}
		});
	}
		
	@FXML
	void cancelButtonClicked(ActionEvent event) {
		Alert cancelAlert = new Alert(AlertType.CONFIRMATION);
    	cancelAlert.setTitle("Cancel");
    	cancelAlert.setHeaderText("Clear the selected file selection?");
    	cancelAlert.getButtonTypes().remove(0, 2);
    	cancelAlert.getButtonTypes().add(0, ButtonType.YES);
    	cancelAlert.getButtonTypes().add(1, ButtonType.NO);
    	Optional<ButtonType> confirmation = cancelAlert.showAndWait();
    	if (confirmation.get() == ButtonType.YES) {
    		fileLocationTextField.clear();
    		cancelAlert.close();
    	} else {
    		cancelAlert.close();
    	}
	}
	
	@FXML
    void backButtonClicked(ActionEvent event) {
		try {
			if (parentMainScene != null) {
	    		Stage currentStage = (Stage)backButton.getScene().getWindow();
	    		currentStage.setScene(parentMainScene);
			}
    	} catch (Exception ex) {
    		Alert warningAlert = new Alert(AlertType.ERROR);
			warningAlert.setTitle("Exception!");
			warningAlert.setHeaderText("Exception: " + ex.toString());
			warningAlert.show();
    	}
    }
}
