package controller;

import java.net.URL;
import java.io.*;
import java.util.*;
import javafx.fxml.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.application.Platform;
import javafx.event.*;
import model.*;

public class MainViewController implements Initializable {
	
	private LabelledImageArrayLoader currentLIArrayLoader = null;
	
	@FXML
	public Button openFileButton;
	
	@FXML
	public Button drawDigitButton;
	
	@FXML
	public Button exitButton;
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		
	}
	
	public void setLIALoader(LabelledImageArrayLoader passedLIALoader) {
		this.currentLIArrayLoader = passedLIALoader;
	}
	
	public void initializeAppLoad() {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				try {
					if (currentLIArrayLoader == null) {
						currentLIArrayLoader = new LabelledImageArrayLoader();
						currentLIArrayLoader.initLIArrayLoad();
					}
				} catch (Exception ex) {
					Alert warningAlert = new Alert(AlertType.ERROR);
					warningAlert.setTitle("Exception!");
					warningAlert.setHeaderText("Exception: " + ex.toString());
					warningAlert.show();
				}
			}
		});		
	}
	
	@FXML
	void openFileButtonClicked(ActionEvent event) {
		try {
    		Stage currentStage = (Stage)openFileButton.getScene().getWindow();
    		FXMLLoader currentOFVCFXMLLoader = new FXMLLoader(getClass().getResource("../view/openfileview.fxml"));
    		Scene currentOFVScene = new Scene((FlowPane)currentOFVCFXMLLoader.load());
    		currentOFVScene.getStylesheets().add("view/application.css");
    		OpenFileViewController currentOFVController = currentOFVCFXMLLoader.<OpenFileViewController>getController();
    		currentOFVController.setLIALoader(currentLIArrayLoader);
    		currentOFVController.setParentScene(currentStage.getScene());
    		currentStage.setScene(currentOFVScene);
    		currentStage.show();    		
    	} catch (IOException ioe) {
    		Alert warningAlert = new Alert(AlertType.ERROR);
			warningAlert.setTitle("Exception!");
			warningAlert.setHeaderText("Exception: " + ioe.toString());
			warningAlert.show();
    	} catch (Exception ex) {
    		Alert warningAlert = new Alert(AlertType.ERROR);
			warningAlert.setTitle("Exception!");
			warningAlert.setHeaderText("Exception: " + ex.toString());
			warningAlert.show();
    	}
	}
	
	@FXML
	void drawDigitButtonClicked(ActionEvent event) {
		try {
    		Stage currentStage = (Stage)drawDigitButton.getScene().getWindow();
    		FXMLLoader currentDDVCFXMLLoader = new FXMLLoader(getClass().getResource("../view/drawdigitview.fxml"));
    		Scene currentDDVScene = new Scene((FlowPane)currentDDVCFXMLLoader.load());
    		currentDDVScene.getStylesheets().add("view/application.css");
    		DrawDigitViewController currentDDVController = currentDDVCFXMLLoader.<DrawDigitViewController>getController();
    		currentDDVController.setLIALoader(currentLIArrayLoader);
    		currentDDVController.setParentScene(currentStage.getScene());
    		currentDDVController.setCanvasControls();
    		currentStage.setScene(currentDDVScene);
    		currentStage.show();    		
    	} catch (IOException ioe) {
    		Alert warningAlert = new Alert(AlertType.ERROR);
			warningAlert.setTitle("Exception!");
			warningAlert.setHeaderText("Exception: " + ioe.toString());
			warningAlert.show();
    	} catch (Exception ex) {
    		Alert warningAlert = new Alert(AlertType.ERROR);
			warningAlert.setTitle("Exception!");
			warningAlert.setHeaderText("Exception: " + ex.toString());
			warningAlert.show();
    	}
	}
	
	@FXML
	void exitButtonClicked(ActionEvent event) {
		try {
			Stage currentStage = (Stage)exitButton.getScene().getWindow();
			currentStage.close();
		} catch (Exception ex) {
			Alert warningAlert = new Alert(AlertType.ERROR);
			warningAlert.setTitle("Exception!");
			warningAlert.setHeaderText("Exception: " + ex.toString());
			warningAlert.show();
    	}
	}
}
