package model;

import java.util.*;

public interface CustomArrayListActions {
	
	public ArrayList<LabelledImage> getLIArrayList();
	public void setArrayList(ArrayList<LabelledImage> providedLIArrayList);
	
}
