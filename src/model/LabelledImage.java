package model;

import java.awt.image.*;

public class LabelledImage {

	// Class members
	private int mnistLabel;
	private BufferedImage mnistImage;
	private double distanceValue;
	
	public LabelledImage() {}
	
	// Getter and setter functions
	public int getMNISTLabel() {
		return mnistLabel;
	}
	
	public void setMNISTLabel(int tempLabel) {
		mnistLabel = tempLabel;
	}
	
	public BufferedImage getMNISTImage() {
		return mnistImage;
	}
	
	public void setMNISTImage(BufferedImage tempImage) {
		mnistImage = tempImage;
	}
	
	public double getDistanceValue() {
		return distanceValue;
	}
	
	public void setDistanceValue(double tempValue) {
		distanceValue = tempValue;
	}
}
