package model;

import java.awt.Image;
import java.awt.Graphics;
import java.awt.image.*;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.*;
import javax.swing.*;

public class ImageOperator implements ProcessImage {

	//Class members
	private BufferedImage currentOperatorImage;
	
	public ImageOperator() {
	}
	
	public ImageOperator(BufferedImage providedImage) {
		this.setImage(providedImage);
	}
	
	public void displayImage() {
		JLabel picLabel = new JLabel(new ImageIcon(getImage()));
		JPanel newPanel = new JPanel();
		newPanel.add(picLabel);
		JFrame newFrame = new JFrame();
		newFrame.setSize(new Dimension(getImage().getWidth(), getImage().getHeight()));
		newFrame.add(newPanel);
		newFrame.setVisible(true);
	}
	
	// Function for internal greyscale image conversion
	public void convertRGBToGrayscale() throws Exception {
		BufferedImage grayscaleImage = null;
		if (this.getImage() != null) {
			BufferedImage currentImage = this.getImage();
			grayscaleImage = new BufferedImage(currentImage.getWidth(), currentImage.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
			ColorConvertOp newCCO = new ColorConvertOp(currentImage.getColorModel().getColorSpace(), 
					grayscaleImage.getColorModel().getColorSpace(), null);
			newCCO.filter(currentImage, grayscaleImage);			
			this.setImage(grayscaleImage);
		}
	}
	
	// Function for grayscale conversion
	public void convertRGBToGreyscale(BufferedImage thisImage) throws Exception {
		BufferedImage greyscaleImage = new BufferedImage(thisImage.getWidth(), thisImage.getHeight(), BufferedImage.TYPE_INT_ARGB);
		for (int y = 0; y < thisImage.getHeight(); y++) {
			for (int x = 0; x < thisImage.getWidth(); x++) {
				int rgbValue = thisImage.getRGB(x, y);
				//Separate values
				int alpha = (rgbValue >> 24) & 0xff;
				int red = (rgbValue >> 16) & 0xff;
				int green = (rgbValue >> 8) & 0xff;
				int blue = (rgbValue) & 0xff;
				
				// provide greyscale correction
				int greyscale = (int)(alpha + (0.3 * red) + (0.59 * green) + (0.11 * blue));
				int newValue = 0xFF000000 | (greyscale << 16) | (greyscale << 8) | (greyscale);
				greyscaleImage.setRGB(x, y, newValue);
			}
		}
		this.setImage(greyscaleImage);
	}
	
	// Function for resizing a grayscale image
	public void resizeSetImage(BufferedImage suppliedImage, int newWidth, int newHeight) throws Exception {
		BufferedImage selectedImage = null;
		BufferedImage tempImage = null;
		selectedImage = suppliedImage;
		java.awt.Image imageForResize = selectedImage.getScaledInstance(newWidth, newHeight, java.awt.Image.SCALE_SMOOTH);
		tempImage = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_ARGB);
		Graphics2D graph2d = tempImage.createGraphics();
		graph2d.drawImage(imageForResize, 0, 0, null);
		graph2d.dispose();
		this.setImage(tempImage);
	}
	
	// Function to get image from a JavaFX canvas and return BufferedImage object
	public void setImageFromCanvas(WritableImage suppliedWImage) throws Exception {
		Graphics tempGraphics = null;
		int currentWidth = (int)suppliedWImage.getWidth();
		int currentHeight = (int)suppliedWImage.getHeight();
		Image tempCanvasImage = SwingFXUtils.fromFXImage(suppliedWImage, null).getScaledInstance(currentWidth, currentHeight, Image.SCALE_SMOOTH);
		BufferedImage tempImage = new BufferedImage(currentWidth, currentHeight, BufferedImage.TYPE_INT_ARGB);
		tempGraphics = tempImage.getGraphics();
		tempGraphics.drawImage(tempCanvasImage, 0, 0, null);
		this.centerAndPadSetImage(tempImage);
		this.convertRGBToGreyscale(this.getImage());
		this.resizeSetImage(this.getImage(), 28, 28);
		tempGraphics.dispose();
	}
	
	// Function for extracting a sub image, finding its center, then padding it and redrawing it after processing
	public void centerAndPadSetImage(BufferedImage imageToBeCentered) throws Exception {
		int minimumX = imageToBeCentered.getWidth();
		int maximumX = 0;
		int minimumY = imageToBeCentered.getHeight();
		int maximumY = 0;
		int suppliedImageWidth = imageToBeCentered.getWidth();
		int suppliedImageHeight = imageToBeCentered.getHeight();
		int iteratedPixelValue = 0;
		for (int y = 0; y < suppliedImageHeight; y++) {
			for (int x = 0; x < suppliedImageWidth; x++) {
				iteratedPixelValue = imageToBeCentered.getRGB(x, y);
                if (iteratedPixelValue == -1) continue;
                if (x < minimumX) {
                	minimumX = x;
                }
                if (x > maximumX) {
                	maximumX = x;
                }
                if (y < minimumY) {
                	minimumY = y;
                }
                if (y > maximumY) {
                	maximumY = y;
                }
			}
		}
		int subImageWidth = maximumX - minimumX;
		int subImageHeight = maximumY - minimumY;
		int subImageWidthCenter = subImageWidth / 2;
		int subImageHeightCenter = subImageHeight / 2;
		BufferedImage tempImage = imageToBeCentered.getSubimage(minimumX, minimumY, subImageWidth, subImageHeight);
		BufferedImage resultImage = new BufferedImage((2 * subImageWidth), (2 * subImageHeight), tempImage.getType());
		Graphics resultImageGraphics = resultImage.getGraphics();
		resultImageGraphics.setColor(Color.WHITE);
		resultImageGraphics.fillRect(0, 0, (2 * subImageWidth), (2 * subImageHeight));
		resultImageGraphics.drawImage(tempImage, subImageWidthCenter, subImageHeightCenter, null);
		resultImageGraphics.dispose();
		this.setImage(resultImage);
	}
	
	// Function for inverting pixel values in a BufferedImage object
	public BufferedImage invertImageColors(BufferedImage imageForInversion) throws NullPointerException {
		BufferedImage invertedImage = new BufferedImage(imageForInversion.getWidth(), imageForInversion.getHeight(), BufferedImage.TYPE_INT_ARGB);
		for (int y = 0; y < imageForInversion.getHeight(); y++) {
			for (int x = 0; x < imageForInversion.getWidth(); x++) {
				int complementedValue = imageForInversion.getRGB(x, y);
				// Invert the colour value, but if alpha value was inverted, add it back to ensure image transparency wasn't impacted
				complementedValue = ~complementedValue | 0xFF000000;
				invertedImage.setRGB(x, y, complementedValue);
			}
		}		
		return invertedImage;
	}
	
	// Getter and setter functions
	public BufferedImage getImage() {
		return this.currentOperatorImage;
	}
	
	public void setImage(BufferedImage providedImage) {
		this.currentOperatorImage = providedImage;
	}
}
