package model;

import java.awt.image.*;

public interface ProcessImage {

	public void convertRGBToGrayscale() throws Exception;
	public void convertRGBToGreyscale(BufferedImage thisImage) throws Exception;
}
