package model;

import java.io.*;
import javax.imageio.*;
import java.awt.*;
import java.awt.image.*;
import javax.swing.*;

//Class for manipulating BufferedImage type
public class ImageFileHandler extends FileHandler {

	// Class members
	int width;
	int height;
	String imageType;
	BufferedImage currentImage; 
	
	public ImageFileHandler() {}
	
	public ImageFileHandler(String fileName) {
		super(fileName);
		currentImage = null;
	}
	
	@Override
	public void readFile() throws IOException {
		if (fp.isFile() && fp.exists()) {
			currentImage = ImageIO.read(fp);
		}
	}

	@Override	
	public void writeFile(String fileName) throws IOException {
		File outputFile = new File(fileName);
		ImageIO.write(currentImage, "jpg", outputFile); 
	}
	
	public void displayImage() {
		//Display loaded image in a frame
		JLabel picLabel = new JLabel(new ImageIcon(currentImage));
		JPanel newPanel = new JPanel();
		newPanel.add(picLabel);
		JFrame newFrame = new JFrame();
		newFrame.setSize(new Dimension(currentImage.getWidth(), currentImage.getHeight()));
		newFrame.add(newPanel);
		newFrame.setVisible(true);
	}
	
	// Getter and setter functions
	public BufferedImage getImage() {
		return this.currentImage;
	}
	
	public void setImage(BufferedImage tempImage) {
		this.currentImage = tempImage;
	}
	
	@Override
	public File getFile() {
		return super.fp;
	}

	@Override
	public void setFile(File file) {
		super.fp = file;
	}

	@Override
	public String getSourceName() {
		return super.sourceName;
	}

	@Override
	public void setSourceName(String sourceName) {
		super.sourceName = sourceName;
	}

	@Override
	public String getDestinationName() {
		return super.destName;
	}

	@Override
	public void setDestinationName(String destName) {
		super.destName = destName;
	}

	@Override
	public int getFileSize() {
		return super.fileSize;
	}

	@Override
	public void setFileSize(int fileSize) {
		super.fileSize = fileSize;
	}

	@Override
	public int getIsDirectory() {
		return super.isDirectory;
	}

	@Override
	public void setIsDirectory(int directoryValue) {
		super.isDirectory = directoryValue;
	}

	@Override
	public String getFileType() {
		return super.fileType;
	}

	@Override
	public void setFileType(String fileType) {
		super.fileType = fileType;
	}

}
