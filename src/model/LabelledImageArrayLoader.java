package model;

import java.io.*;
import java.util.*;
import java.awt.image.*;
import javafx.scene.image.*;

public class LabelledImageArrayLoader implements CustomArrayListActions{
	
	// Class members 
	private ArrayList<LabelledImage> currentLIArrayList;
	private int recognizedDigit;
	private ImageOperator currentIOperator = null;
	private int loadedLabelCount;
	
	public LabelledImageArrayLoader() {
		this.currentIOperator = new ImageOperator();
	}
	
	// Function for sorting ArrayList
	public void sortArrayList() {
		currentLIArrayList.sort(new SortByDistanceAsc());;
	}
	
	// Function for custom object ArrayList loading
	public void initLIArrayLoad() throws Exception {
		FileInputStream inputStreamLabels = null;
		FileInputStream inputStreamImages = null;
		DataInputStream dataStreamLabels = null;
		DataInputStream dataStreamImages = null;			
		// Data Stream implementation
		// Set new FileInputStreams
		inputStreamLabels = new FileInputStream(new File("rsc/train-labels.idx1-ubyte"));
		inputStreamImages = new FileInputStream(new File("rsc/train-images.idx3-ubyte"));
		// Add FileInputStreams to DataInputStreams
		dataStreamLabels = new DataInputStream(inputStreamLabels);
		dataStreamImages = new DataInputStream(inputStreamImages);
		// Get idx file header information for processing
		int labelStartCode = dataStreamLabels.readInt();
		int labelCount = dataStreamLabels.readInt();
		this.setLoadedLabelCount(labelCount);
		int imageStartCode = dataStreamImages.readInt();
		int imageCount = dataStreamImages.readInt();
		int imageHeight = dataStreamImages.readInt();
		int imageWidth = dataStreamImages.readInt();
		
		// Initialize ArrayList
		currentLIArrayList = new ArrayList<LabelledImage>(labelCount);
		
		// Array creation for label and image data
		int imageSize = imageHeight * imageWidth;
		byte[] labelData = new byte[labelCount];
		byte[] imageData = new byte[imageSize * imageCount];
		BufferedImage tempImage;
		
		// Fill byte arrays with file data
		dataStreamLabels.read(labelData);
		dataStreamImages.read(imageData);
		
		for (int currentRecord = 0; currentRecord < labelCount; currentRecord++) {
							
			// Get individual label number
			int currentLabel = labelData[currentRecord];
			// Create instance of custom class
			LabelledImage newLabelledImage = new LabelledImage();
			// Set label attribute of custom object
			newLabelledImage.setMNISTLabel(currentLabel);
			
			/*
			tempImage = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_BYTE_GRAY);
			WritableRaster tempImageRaster = tempImage.getRaster();				
			// Get individual image data
			int imageSize = imageHeight * imageWidth;
			imageData = new int[imageSize];
			// Process image data
			for(int currentPixel = 0; currentPixel < imageSize; currentPixel++) {
				//image data read in greyscale
				int currentPixelValue = inputStreamImages.read();
				imageData[currentPixel] = currentPixelValue;
			}
			int arrayPos = 0;
			// Provide raster sample of 1 integer
			int[] currentSample = new int[1];
			int arraySize = imageHeight * imageWidth;
			for (int y = 0; y < imageHeight; y++) {
				for (int x = 0; x < imageWidth; x++) {
					arrayPos = arrayPos + (x + y);
					if (arrayPos < arraySize) {
						currentSample[0] = imageData[arrayPos];
						tempImageRaster.setPixel(x, y, currentSample);
					}
				}
			}
			// Set data for temporary BufferedImage
			tempImage.setData(tempImageRaster);
			*/
			
			// Create temporary BufferedImage object for image processing
			tempImage = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_ARGB);
			
			int[][] imageDataArray = new int[imageWidth][imageHeight];				
			for (int row = 0; row < imageHeight; row++) {					
				for(int column = 0; column < imageWidth; column++) {
					imageDataArray[column][row] = imageData[(currentRecord * imageSize) + ((row * imageWidth) + column)] | 0xFF000000;						
					tempImage.setRGB(column, row, imageDataArray[column][row]);
				}					
			}
			// Since the test images used are black text on a white background, invert the image colours before the array is loaded
			// to improve comparison accuracy
			BufferedImage currentInvertedImage = this.getIOperator().invertImageColors(tempImage);
			newLabelledImage.setMNISTImage(currentInvertedImage);
			// Add newly created LabelledImage object to created array WITHOUT distance value added - this will be calculated later
			currentLIArrayList.add(newLabelledImage);
		}
		if (inputStreamLabels != null) {
			inputStreamLabels.close();
		}
		if (inputStreamImages != null) {
			inputStreamImages.close();
		}
	}
	
	// Function for computing Euclidean distance 
	public void computeEDistanceForArrayList() throws NullPointerException {
		ArrayList<LabelledImage> tempLIAList = this.getLIArrayList();
		int currentLIAItemTotal = tempLIAList.size();
		BufferedImage imageToCompare = this.getIOperatorImage();
		if (imageToCompare != null) {
			int currentImageWidth = imageToCompare.getWidth();
			int currentImageHeight = imageToCompare.getHeight();
			double distanceValue = 0.0;
			double squaredSum = 0.0;
			// Iterate over loadedLIArrayList
			for (int i = 0; i < currentLIAItemTotal; i++) {					
				// Fetch LabelledImage object at index
				LabelledImage currentComparisonLabelledImage = tempLIAList.get(i);
				// Get BufferedImage from LabelledImage class for pixel comparison
				BufferedImage currentComparisonImage = currentComparisonLabelledImage.getMNISTImage();
				// Iterate through pixel array, comparing image retrieved with image from ArrayList
				for (int y = 0; y < currentImageHeight; y++) {
					for (int x = 0; x < currentImageWidth; x++) {
						int imageToComparePixelValue = imageToCompare.getRGB(x, y);
						int currentComparisonImagePixelValue = currentComparisonImage.getRGB(x, y);
						// Calculate squared sum of difference and add to total
						squaredSum += Math.pow((imageToComparePixelValue - currentComparisonImagePixelValue), 2);
					}
				}
				// Calculate Euclidean distance value
				distanceValue = Math.sqrt(squaredSum);
				// Assign value to LabelledImage object attribute for distance
				currentComparisonLabelledImage.setDistanceValue(distanceValue);
				tempLIAList.add(currentComparisonLabelledImage);
			}
			this.setArrayList(tempLIAList);
			this.sortArrayList();
		}
	}
	
	// Function for obtaining confidence ratio
	public double getConfidenceRatio(int k) throws Exception {
		int maxCountLabel = 0;
		int maxCount = 0;
		this.sortArrayList();
		LabelledImage[] tempArray = new LabelledImage[k];
		int differenceCount = 1;
		
		// Temporary array for keeping track of which numbers were identified within k number of neighbours
		for (int firstIterator = 0; firstIterator < k; firstIterator++)
		{
			LabelledImage LIatIndex = this.getLIArrayList().get(firstIterator);
			tempArray[firstIterator] = LIatIndex;
		}
		
		// Temporary array for keeping track of the number of different digits with the closest distance
		for (int secondIterator = 0; secondIterator < k; secondIterator++) {
			if (secondIterator > 0) {
				LabelledImage objectAtCurrentIndex = tempArray[secondIterator];
				LabelledImage objectAtPreviousIndex = tempArray[secondIterator - 1];
				if(objectAtCurrentIndex.getMNISTLabel() != objectAtPreviousIndex.getMNISTLabel()) {
					differenceCount++; 
				}		
			}
		}
		
		// 3 arrays for keeping track of digits, their counts and their closest distance
		// [label][count][closest distance]
		int[] labelTrackingArray = new int[differenceCount];
		int[] countTrackingArray = new int[differenceCount];
		double[] distanceTrackingArray = new double[differenceCount];
		int count = 1;
		int internalDifferenceCount = 0;
		for (int thirdIterator = 0; thirdIterator < k; thirdIterator++) {
			if (internalDifferenceCount <= differenceCount) {
				int trackedLabel = tempArray[thirdIterator].getMNISTLabel();
				double trackedDistance = tempArray[thirdIterator].getDistanceValue();
				if (thirdIterator > 0) {
					LabelledImage selectedObjectAtCurrentIndex = tempArray[thirdIterator];
					LabelledImage selectedObjectAtPreviousIndex = tempArray[thirdIterator - 1];
					if(selectedObjectAtCurrentIndex.getMNISTLabel() == selectedObjectAtPreviousIndex.getMNISTLabel()) {
						count++;
					} else {
						trackedLabel = selectedObjectAtCurrentIndex.getMNISTLabel();
						trackedDistance = selectedObjectAtCurrentIndex.getDistanceValue();
						count = 1;
						internalDifferenceCount++;
					}
				}
				labelTrackingArray[internalDifferenceCount] = trackedLabel;			
				countTrackingArray[internalDifferenceCount] = count;
				distanceTrackingArray[internalDifferenceCount] = trackedDistance;
			}
		}
		
		// Iterate over arrays synchronously and get nearest neighbour by count and distance			
		for (int fourthIterator = 0; fourthIterator < differenceCount; fourthIterator++) {
			if (fourthIterator > 0) {
				if(distanceTrackingArray[fourthIterator - 1] < distanceTrackingArray[fourthIterator]) {
					if (countTrackingArray[fourthIterator] >= countTrackingArray[fourthIterator - 1]) {
						maxCountLabel = labelTrackingArray[fourthIterator - 1];
						maxCount = countTrackingArray[fourthIterator - 1];
					}
				} else {
					maxCountLabel = labelTrackingArray[fourthIterator];
					maxCount = countTrackingArray[fourthIterator];
				}				
			} else {
				maxCountLabel = labelTrackingArray[fourthIterator];
				maxCount = countTrackingArray[fourthIterator];
			}
		}		
		// set recognized digit after calculation
		setRecognizedDigit(maxCountLabel);
		double confidenceRatio = 100 - (((double)maxCount / k) * 100);
		return confidenceRatio;
	}
	
	// Getter and setter functions
	public ArrayList<LabelledImage> getLIArrayList() {
		return this.currentLIArrayList;
	}
	
	public void setArrayList(ArrayList<LabelledImage> providedLIArrayList) {
		this.currentLIArrayList = providedLIArrayList;
	}
	
	public ImageOperator getIOperator() {
		return this.currentIOperator;
	}
	
	public void setIOperator(ImageOperator providedIOperator) {
		this.currentIOperator = providedIOperator;
	}
	
	public BufferedImage getIOperatorImage() {
		return this.currentIOperator.getImage();
	}
	
	public void setIOperatorImage(BufferedImage suppliedImage) {
		this.currentIOperator.setImage(suppliedImage);
	}
	
	public void setIOperatorImageFromCanvas(WritableImage suppliedWImage) throws Exception {
		this.currentIOperator.setImageFromCanvas(suppliedWImage);
	}
	
	public int getRecognizedDigit() {
		return this.recognizedDigit;
	}
	
	public void setRecognizedDigit(int suppliedDigit) {
		this.recognizedDigit = suppliedDigit;
	}
	
	public int getLoadedLabelCount() {
		return this.loadedLabelCount;
	}
	
	public void setLoadedLabelCount(int providedLabelCount) {
		this.loadedLabelCount = providedLabelCount;
	}
}
