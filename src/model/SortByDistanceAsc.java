package model;

import java.util.*;

public class SortByDistanceAsc implements Comparator<LabelledImage>{
	
	public int compare(LabelledImage a, LabelledImage b)
	{
		return (int)(a.getDistanceValue() - b.getDistanceValue());
	}
}
