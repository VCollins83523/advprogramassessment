package model;

import java.io.*;

public abstract class FileHandler {
	
	// Class members
	File fp;
	String sourceName;
	String destName;
	int fileSize;
	int isDirectory;
	String fileType;
	
	public FileHandler() {}
	public FileHandler(String source_name) {
		this.sourceName = source_name;
		this.fp = new File(this.sourceName);
	}
	
	public FileHandler(String source_file, String dest_file) {
		this.sourceName = source_file;
		this.destName = dest_file;
		this.fp = new File(this.sourceName);
	}
	
	public abstract void readFile() throws IOException;
	
	public abstract void writeFile(String fileName) throws IOException;
	
	// Getter and setter functions
	public abstract File getFile();
	
	public abstract void setFile(File file);
	
	public abstract String getSourceName();
	
	public abstract void setSourceName(String sourceName);
	
	public abstract String getDestinationName();
	
	public abstract void setDestinationName(String destName);
	
	public abstract int getFileSize();
	
	public abstract void setFileSize(int fileSize);
	
	public abstract int getIsDirectory();
	
	public abstract void setIsDirectory(int DirectoryValue);
	
	public abstract String getFileType();
	
	public abstract void setFileType(String fileType);
}
